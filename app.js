const express = require("express");
const bodyParser = require("body-parser");
const staticPublicDir = process.cwd() + "/public";
const PORT = 3000;
const SerialPort = require("serialport");

const portAddrs = ['/dev/tty.usbmodem1411', '/dev/tty.usbmodem1411'];
let ports = [];
for(var i = 0 ; i < portAddrs.length; i++){
    ports.push(new SerialPort(portAddrs[i]));
}

function writeToPort(str){
    for(var i = 0 ; i < ports.length; i++){
        ports[i].write(str);
    }
}


let app = express();
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json())

app.use(express.static(staticPublicDir, {
    extensions: ['html', 'htm']
}));

app.get("/video_ended", function(req,res){
    console.log("--->", new Date(), " --- video ended"); 
    writeToPort("0")
    res.json({status: true, message: "yeah!"});
})

app.listen(PORT, () => console.log(`>> Server listening on PORT ${PORT}`));
